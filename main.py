#Configuración y módulos
import tkinter as tk
import webbrowser
from tkinter import *
from tkinter import ttk 
from login import (
    userclass,
)
root = Tk() 

username = StringVar()
pwname = StringVar()

def createGUI():
    #Ventana Principal
    marco_primario = Frame()
    marco_primario.place(x=0,y=0)
    marco_primario.config(width="1600", height="350")
    marco_primario.config(bg='white')

    root.title("Dogtor Health")
    root.geometry("1600x900")
    root.resizable(width=False, height=False)
    root.config(bg='#BDF0E4')


    #Color primario y logo
    logo = PhotoImage(file="images/logo1.png")
    fondo = Label(image=logo, borderwidth=0).place(x=350,y=0) 


    #Textos y títulos
    user = Label(root, text="Login")
    user.config(bg='#BDF0E4', fg='#ffffff', font=("Verdana",25))
    user.place(x=770,y=400)

    user = Label(root, text="Usuario*")
    user.config(bg='#BDF0E4', fg='#058981', font=("Verdana",12))
    user.place(x=620,y=500)

    username.set("")
    Euser = Entry(root, borderwidth=0, textvariable=username)
    Euser.place(x=620, y=525, height=40, width = 400)

    pw = Label(root, text="Contraseña*")
    pw.config(bg='#BDF0E4', fg='#058981', font=("Verdana",12))
    pw.place(x=620, y=590)

    pwname.set("")
    Epw = Entry(root, width = 30, borderwidth=0,textvariable=pwname,show="*")
    Epw.place(x=620, y=615, height=40, width = 400)

    def initSesion():
        userLabel.connect(pwname.get())


    #Boton de Login
    boton_login = ttk.Button(root, text ="LOGIN", command=initSesion)
    #,borderwidth=0, fg='#ffffff', bg='#5AC2A9', font=("Verdana",12) - Configuración de Boton no ttk
    boton_login.place(x=620, y=693, height=40, width = 90)


    #Check Recordarme
    check = Checkbutton(root, text="Recuérdame", bg='#BDF0E4', borderwidth=0, font=("Verdana",11), fg='#8D8484')
    check.place(x=750, y=700)

    def callback(url):
        webbrowser.open_new(url)


    #olvidó su contraseña
    forgot = Label(root, text="¿Olvidaste tu contraseña?", bg='#BDF0E4', font=("Verdana",11), fg='#8D8484')
    forgot.place(x=620, y=750)
    forgot.bind("<Button-1>", lambda e: callback("/home/eli/dogtorhealth/images/forgot.py"))
    """Color secundario y formulario de ingreso"""

    root.mainloop()

if __name__=="__main__":
    userLabel = userclass("Eli","1234")
    createGUI()
    






